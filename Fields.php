<?php
namespace LibMysql{
    abstract class Type{
        const Str = 1;
        const Int = 2;
        const None = 3;
    }

    class Fields {
        private $intList = array();
        private $strList = array();
        private $noneList = array();

        private $additional = array();

        public function __construct( $config = array() ){
            $this->config($config);
        }

        private function config($config = array() ){
            foreach($config as $key => $row){
                if($row[1] === Type::Int){
                    $this->intList[$key] = $row[0];
                }
                
                if($row[1] === Type::Str){
                    $this->strList[$key] = $row[0];
                }

                if($row[1] === Type::None){
                    $this->noneList[$key] = $row[0];
                }

                if(isset($row[2])){
                    $this->additional[] = $row[2];
                }
            }
        }

        public function getAdditional(){
            return $this->additional;
        }

        public function getFieldsWithData(){
            return array_merge( 
                $this->intList,
                $this->strList
            );
        }

        public function getInt(){
            return $this->intList;
        }

        public function getStr(){
            return $this->strList;
        }

        public function getNone(){
            return $this->noneList;
        }
    }
}
?>