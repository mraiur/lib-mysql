<?php
namespace LibMysql{
    class Update extends Query{
        private $table=null;
        private $fieldsObject;
        private $insertType = InsertType::Single;

        public function __construct( $table = null){
            if( $table === null ){
                throw new \Exception("MysqlLib Update no passed tablename", 1);
            } else{
                $this->table = $table;
            }
        }

        public function buildQuery(){
            $this->query = 'UPDATE '."\n";

            $this->query .= "\t".$this->table."\n";

            $this->query .= 'SET'."\n";

            $data = $this->fieldsObject->getFieldsWithData();
            $keys = array_keys($data);
            $list = array();

            foreach( $keys as $key ){
                $list[] = $key.' = :'.$key;
            }

            $this->query .= implode(",\n", $list);

            $this->query .= "\n".'WHERE'."\n";

            $this->query .= $this->where;
        }

        public function bindParams(){
            
            $this->data['int'] = $this->fieldsObject->getInt();
            $this->data['str'] = $this->fieldsObject->getStr();
            $this->data['all'] = array_merge(
                $this->data['int'],
                $this->data['str'],
                $this->whereInt,
                $this->whereStr
            );
            $this->debugParams();

            foreach($this->data['int'] as $key => $value){
                $this->statement->bindValue($key, $value, \PDO::PARAM_INT);
            }

            foreach($this->data['str'] as $key => $value){
                $this->statement->bindValue($key, $value, \PDO::PARAM_STR);
            }

            foreach($this->whereInt as $key => $value){
                $this->statement->bindValue($key, $value, \PDO::PARAM_INT);
            }

            foreach($this->whereStr as $key => $value){
                $this->statement->bindValue($key, $value, \PDO::PARAM_STR);
            }
        }

        public function postExec( $status ){
            if($status){
                return $status;
            }
            return false;
        }

        public function values( $fieldsObject = null ){
            if( $fieldsObject === null ){
                throw new \Exception("MysqlLib Update passed null config, expected Fields object ", 1);
            }

            $this->fieldsObject = $fieldsObject;

            return $this;
        }
    }
}
?>