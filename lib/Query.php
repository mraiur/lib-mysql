<?php
namespace LibMysql{
    class Query{
        public $query = ""; 
        public $select = null;
        public $from = null;
        public $where = null;
        public $whereInt = array();
        public $whereStr = array();
        public $data = array(
            'all' => null
        );
        public $group = null;
        public $limit = null;
        public $order = null;

        public $statement;
        public $DEBUG = false;
        
        public function connection(){
            return Connect::getInstance()->connection;
        }


        public function limit($offset = 0, $limit = 20){
            $this->limit = $offset.",".$limit;
        }

        public function from( $from = "*" ){
            if( is_string($from) ){
                $this->from = $from;
            } elseif ( is_array($from )) {
                $this->from = implode('  ', $from);
            }
            return $this;
        }

        public function where( $where = ""){
            if( is_string($where) ){
                $this->where = $where;
            } elseif ( is_array($where )) {

                $whereList = array();
                foreach($where as $key => $row){
                    if(isset($row[2])){
                        $whereList[] = $row[2];
                    }

                    if($row[1] === Type::Int){
                        $this->whereInt[$key] = $row[0];
                    }
                    
                    if($row[1] === Type::Str){
                        $this->whereStr[$key] = $row[0];
                    }
                }
                $this->where = implode(" \n", $whereList);
            }

            return $this;   
        }

        public function debugParams(){
            if($this->DEBUG){
                echo 'DATA<pre>'.print_r($this->data, true).'<pre>';
                echo 'WHERE INT <pre>'.print_r($this->whereInt, true).'<pre>';
                echo 'WHERE STR <pre>'.print_r($this->whereStr, true).'<pre>';
            }
        }

        public function bindParams(){
            $this->debugParams();
            // Implement in chield classes
        }

        public function execQuery(){
            $this->statement = $this->connection()->prepare($this->query);
            $this->bindParams();
            return $this->statement->execute($this->data['all']);
        }

        public function postExec( $queryResult ){
            // Overwrite in subclasses
            return $queryResult;
        }

        public function exec($debug = false){
            $this->DEBUG = $debug;
            if(method_exists($this, 'buildQuery')){
                $this->buildQuery();
                if($this->DEBUG){
                    echo '<pre>'.$this->query.'</pre>';
                }
                return $this->postExec( $this->execQuery() );
            } else {
                throw new \Exception("MysqlLib buildQuery not implemented", 1);
            }
        }
    }
}
?>