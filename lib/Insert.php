<?php
namespace LibMysql{
    abstract class InsertType{
        const Single = 1;
        const Batch = 2;
    }

    class Insert extends Query{
        private $table=null;
        private $fieldsObject;
        private $insertType = InsertType::Single;

        public function __construct( $table = null){
            if( $table === null ){
                throw new \Exception("MysqlLib Insert no passed tablename", 1);
            } else{
                $this->table = $table;
            }
        }

        public function buildQuery(){
            $this->query = 'INSERT INTO '."\n";

            $this->query .= "\t".$this->table."\n";

            if( $this->insertType === InsertType::Single ){
                $this->query .= 'SET'."\n";

                $data = $this->fieldsObject->getFieldsWithData();
                $keys = array_keys($data);
                $list = array();

                foreach( $keys as $key ){
                    $list[] = $key.' = :'.$key;
                }

                $direct = $this->fieldsObject->getNone();
                foreach( $direct as $key => $value ){
                    $list[] = $key.' = '.$value;
                }

                $this->query .= implode(",\n", $list);
            }
        }

        public function bindParams(){
            $this->data['int'] = $this->fieldsObject->getInt();
            $this->data['str'] = $this->fieldsObject->getStr();

            $this->debugParams();

            foreach($this->data['int'] as $key => $value){
                $this->statement->bindValue($key, $value, \PDO::PARAM_INT);
            }

            foreach($this->data['str'] as $key => $value){
                $this->statement->bindValue($key, $value, \PDO::PARAM_STR);
            }

            $this->data['all'] = array_merge($this->data['int'], $this->data['str']);
        }

        public function postExec( $status ){
            if($status){
                $id = $this->connection()->lastInsertId();
                return $id;
            }
            return false;
        }

        public function single( $fieldsObject = null ){
            if( $fieldsObject === null ){
                throw new \Exception("MysqlLib Insert single passed null config, expected Fields object ", 1);
            }

            $this->fieldsObject = $fieldsObject;

            return $this;
        }

        public function batch(){
            $this->insertType = InsertType::Batch;
            //TODO do batch insert
        }
    }
}
?>