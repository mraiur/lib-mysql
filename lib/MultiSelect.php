<?php
namespace LibMysql{
    class MultiSelect extends Select{
        public function postExec( $queryResult ){
            if($queryResult){
                return $this->statement->fetchAll();
            }
            return false;
        }
    }
}
?>