<?php
namespace LibMysql{
    class Select extends Query{
        public function __construct($select = "*"){
            $this->select = $select;
        }

        public function postExec( $queryResult ){
            if($queryResult){
                return $this->statement->fetch();
            }
            return false;
        }

        public function bindParams(){
            
            foreach($this->whereInt as $key => $value){
                $this->statement->bindValue($key, $value, \PDO::PARAM_INT);
            }

            foreach($this->whereStr as $key => $value){
                $this->statement->bindValue($key, $value, \PDO::PARAM_STR);
            }
            $this->debugParams();
        }

        public function buildQuery(){
            $this->query = 'SELECT '."\n";
            if($this->select !== null ){
                $this->query .= $this->select."\n";
            }

            if($this->from !== null ){
                $this->query .= 'FROM '."\n".$this->from."\n";
            }

            if($this->where !== null ){
                $this->query .= 'WHERE '."\n".$this->where."\n";
            }

            if($this->group !== null ){
                $this->query .= 'GROUP BY '."\n".$this->group."\n";
            }

            if($this->limit !== null ){
                $this->query .= 'LIMIT '."\n".$this->limit."\n";
            }

            if($this->order !== null ){
                $this->query .= 'ORDER BY '."\n".$this->order."\n";
            }
        }
    }
}
?>