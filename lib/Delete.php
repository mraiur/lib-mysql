<?php
namespace LibMysql{
    class Delete extends Query{
        public function __construct( $from = null){
            if( $from === null ){
                throw new \Exception("MysqlLib Delete no passed tablename", 1);
            } else{
                $this->from = $from;
            }
        }

        public function bindParams(){
            $this->debugParams();
            foreach($this->whereInt as $key => $value){
                $this->statement->bindValue($key, $value, \PDO::PARAM_INT);
            }

            foreach($this->whereStr as $key => $value){
                $this->statement->bindValue($key, $value, \PDO::PARAM_STR);
            }
        }

        public function buildQuery(){
            $this->query = 'DELETE '."\n";
            
            if($this->from !== null ){
                $this->query .= 'FROM '."\n".$this->from."\n";
            }

            if($this->where !== null ){
                $this->query .= 'WHERE '."\n".$this->where."\n";
            }
        }
    }
}
?>