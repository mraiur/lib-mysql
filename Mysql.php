<?php
namespace LibMysql{

    require_once dirname(__FILE__).'/Fields.php';
    require_once dirname(__FILE__).'/lib/Query.php';
    require_once dirname(__FILE__).'/lib/Delete.php';
    require_once dirname(__FILE__).'/lib/Insert.php';
    require_once dirname(__FILE__).'/lib/Select.php';
    require_once dirname(__FILE__).'/lib/MultiSelect.php';
    require_once dirname(__FILE__).'/lib/Update.php';
    use \PDO;

    class Config{
        public static $config = array(
            'database' => null, 
            'host' => 'localhost', 
            'password' => null, 
            'user' => null,
            'encoding' => 'UTF8'
        );

        public static function set($config = array()){
            self::$config = array_merge(self::$config, $config );
        }

        public static function get($name){
            return self::$config[$name];
        }
    }

    class Connect{
        
        public static $instance = null;
        public $connection = null;
        

        public static function getInstance(){

            if(self::$instance === null){
                $dsn = "mysql:dbname=".Config::get('database').";host=".Config::get('host')."";
                $obj = new Connect();
                $obj->connection = new \PDO($dsn, Config::get('user'), Config::get('password'),  array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \''.Config::get('encoding').'\''));
                self::$instance = $obj;
            }
            return self::$instance;
        }

        public static function MultiSelect($what = "*"){
            return new \LibMysql\MultiSelect($what);
        }

        public static function Select($what = "*"){
            return new \LibMysql\Select($what);
        }

        public static function Insert($where){
            return new \LibMysql\Insert($where);   
        }

        public static function Update($where){
            return new \LibMysql\Update($where);   
        }

        public static function Delete($where){
            return new \LibMysql\Delete($where);   
        }
    }

    function Select(){
        return call_user_func_array("LibMysql\Connect::Select", func_get_args() );
    }

    function MultiSelect(){
        return call_user_func_array("LibMysql\Connect::MultiSelect", func_get_args() );
    }

    function Insert(){
        return call_user_func_array("LibMysql\Connect::Insert", func_get_args() );
    }

    function Update(){
        return call_user_func_array("LibMysql\Connect::Update", func_get_args() );
    }

    function Delete(){
        return call_user_func_array("LibMysql\Connect::Delete", func_get_args() );
    }
}
?>